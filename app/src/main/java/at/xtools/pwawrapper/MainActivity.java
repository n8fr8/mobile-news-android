package at.xtools.pwawrapper;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.webkit.ProxyConfig;
import androidx.webkit.ProxyController;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.text.TextUtils;
import android.widget.EditText;

import com.clostra.newnode.NewNode;

import java.net.MalformedURLException;
import java.util.concurrent.Executor;

import at.xtools.pwawrapper.ui.UIManager;
import at.xtools.pwawrapper.webview.WebViewHelper;
import info.guardianproject.netcipher.NetCipher;
import info.guardianproject.netcipher.proxy.OrbotHelper;
import info.guardianproject.netcipher.proxy.StatusCallback;

public class MainActivity extends AppCompatActivity {
    // Globals
    private UIManager uiManager;
    private WebViewHelper webViewHelper;
    private boolean intentHandled = false;

    private boolean mUseNewNode = true;
    private boolean mEnableCircumvention = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Setup Theme
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String appKey = "appUrl";
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        // Setup Helpers
        uiManager = new UIManager(this);
        webViewHelper = new WebViewHelper(this, uiManager);

        // Setup App
        webViewHelper.setupWebView();
   //     uiManager.changeRecentAppsIcon();

        // Check for Intents
        try {
            Intent i = getIntent();
            String intentAction = i.getAction();
            // Handle URLs opened in Browser
             if (!intentHandled && intentAction != null && intentAction.equals(Intent.ACTION_VIEW)){
                    Uri intentUri = i.getData();
                    String intentText = "";
                    if (intentUri != null){
                        intentText = intentUri.toString();
                    }
                    // Load up the URL specified in the Intent
                    if (!intentText.equals("")) {
                        intentHandled = true;
                        webViewHelper.loadIntentUrl(intentText);
                    }
             } else {

                 String appUrl = getString(R.string.app_url);
                 if (TextUtils.isEmpty(appUrl))
                     appUrl = prefs.getString(appKey,"");

                 if (TextUtils.isEmpty(appUrl))
                 {
                     AlertDialog.Builder builder = new AlertDialog.Builder(this);
                     builder.setTitle("Enter Test URL");

// Set up the input
                     final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                     input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_URI);
                     builder.setView(input);

// Set up the buttons
                     builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {

                             try {
                                 String appUrl = input.getText().toString();
                                 webViewHelper.setAppUrl(appUrl);
                                 webViewHelper.loadHome();

                                 SharedPreferences.Editor editor = prefs.edit();
                                 editor.putString(appKey, appUrl);
                                 editor.commit();

                             } catch (MalformedURLException e) {
                                 e.printStackTrace();
                             }
                         }
                     });
                     builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {
                             dialog.cancel();
                         }
                     });

                     builder.show();
                 }
                 else {
                     // Load up the Web App
                     webViewHelper.setAppUrl(appUrl);
                     webViewHelper.loadHome();
                 }
             }
        } catch (Exception e) {
            // Load up the Web App
            webViewHelper.loadHome();
        }

        if (mEnableCircumvention) {

            if (OrbotHelper.isOrbotInstalled(this)) {

                //enable proxy status listener
                OrbotHelper.get(this).addStatusCallback(new StatusCallback() {
                    @Override
                    public void onEnabled(Intent statusIntent) {

                    }

                    @Override
                    public void onStarting() {

                    }

                    @Override
                    public void onStopping() {

                    }

                    @Override
                    public void onDisabled() {

                    }

                    @Override
                    public void onStatusTimeout() {

                    }

                    @Override
                    public void onNotYetInstalled() {

                    }
                });

                //enable OrbotHelper

                OrbotHelper.get(this).init();

                //set proxy
                setProxy();
            }
            else  if (mUseNewNode) {
                NewNode.setRequestDiscoveryPermission(false);
                NewNode.setLogLevel(0);
                NewNode.init();
                webViewHelper.setUsingCircumvention("newnode");
            }
        }

    }

    private void setProxy() {

        NetCipher.setProxy(NetCipher.ORBOT_HTTP_PROXY);
        webViewHelper.setUsingCircumvention("tor");

        ProxyConfig proxyConfig = new ProxyConfig.Builder()
                .addProxyRule("127.0.0.1:8118") //http proxy for tor
                .addDirect().build();
        ProxyController.getInstance().setProxyOverride(proxyConfig, new Executor() {
            @Override
            public void execute(Runnable command) {
                //do nothing
            }
        }, new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    @Override
    protected void onPause() {
        webViewHelper.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        webViewHelper.onResume();
        // retrieve content from cache primarily if not connected
        webViewHelper.forceCacheIfOffline();
        super.onResume();
    }

    // Handle back-press in browser
    @Override
    public void onBackPressed() {
        if (!webViewHelper.goBack()) {
            super.onBackPressed();
        }
    }
}
